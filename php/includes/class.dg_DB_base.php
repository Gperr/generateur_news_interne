<?php
class dg_DB_base {
	
	protected $host;
	protected $user;
	protected $pass;
	protected $bdd;
	private $link;
	private $result;
	private $erreur;

 	function __construct()
	{
		  $this->DB_Connect();
	}
			
	private function DB_Connect() 
	{
        $this->link = mysql_connect($this->host,$this->user,$this->pass) or die("Impossible de se connecter : " . mysql_error());
        @mysql_select_db($this->bdd,$this->link) or die("Impossible de sÃ©lectioner la base");
	}
			
	public function DB_Close()
	{
		mysql_close($this->link);
	}
			
	public function DB_Query($query)
	{
		$this->erreur="";
		$this->result=mysql_query($query,$this->link)
		or $this->erreur=mysql_errno()." : ".mysql_error();
		if ($this->erreur) die($this->erreur);
		return $this->result;
	}
			
	public function DB_Free_result($result_existant='')
	{
		if(!$this->result) 
		{
 			echo $this->erreur;
			return;
      	} 
		else if ($result_existant!='')
		{
    		$res=mysql_free_result($result_existant);
    		return $res;
		} 
		else 
		{
  			$res=mysql_free_result($this->result);
  			return $res;
		}
	}
			
			
	public function DB_Query_lastid()
	{
		$this->erreur='';
		$this->result=mysql_query($query,$this->link)
		or $this->erreur=mysql_errno($this->link).' : '.mysql_error($this->link);
		return mysql_insert_id($this->link);
	}
			
	public function DB_FetchArray($result_existant='')
	{
    	if(!$this->result) 
		{
 			 echo $this->erreur;
			  return;
      	} 
		else if ($result_existant!='') 
		{
    		$res=mysql_fetch_array($result_existant);
    		return $res;
		}
		else 
		{
  			$res=mysql_fetch_array($this->result);
  			return $res;
		}
	}
	
	public function DB_FetchObject($result_existant='')
	{
    	if(!$this->result) 
		{
 			 echo $this->erreur;
			  return;
      	} 
		else if ($result_existant!='') 
		{
    		$res=mysql_fetch_object($result_existant);
    		return $res;
		}
		else 
		{
  			$res=mysql_fetch_object($this->result);
  			return $res;
		}
	}
			
	public function DB_NumRows()
	{
  		if(!$this->result)
		{
			$res=0;
      	} 
		else 
		{
   			$res=mysql_num_rows($this->result);
		}
		return $res;
	}
			
	public function DB_LastInsertId()
	{
		if(!$this->result)
		{
			return;
        }
		$res=mysql_insert_id($this->link);
		return $res;
	}

	public function DB_Affected_Rows()
	{
		if(!$this->result)
		{
			return;
        }
		$res=mysql_affected_rows($this->link);
		return $res;
	}
			
	public function DB_Mk_Insert_Request($table, $fields, $values, $ignore=false)
	{
		$res='INSERT ';
		if ($ignore) $res.='IGNORE ';
		$res.='INTO '.$table. ' (';
		$nb=sizeof($fields);
		$val='';
		for ($i=0; $i<$nb; $i++) 
		{
			if ($i>0) { $res.=', '; $val.=', '; }
			$res.=$fields[$i];
			if ($values[$i]==='@now') $val.='now()';
			else $val.='"'.$values[$i].'"';
		}
		$res.=') values ('.$val.')';
		return($res);
	}
			
	public function DB_Mk_Update_Request($table, $fields, $values, $whereuid) 
	{
		$res='UPDATE '.$table. ' set ';
		$nb=sizeof($fields);
		$val='';
		for ($i=0; $i<$nb; $i++) 
		{
			if ($i>0) { $res.=', '; $val.=', '; }
			$res.=$fields[$i].'=';
			if ($values[$i]==='@now') $val='now()';
			else $val='"'.$values[$i].'"';
			$res.=$val;
		}
		$res.=' '.$whereuid;
		return($res);
	}		
}

?>