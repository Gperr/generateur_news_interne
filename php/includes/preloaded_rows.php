<div id="newsletter-preloaded-rows">
    <!-- ===== HEADER SECTION ===== -->
    <!-- ===== LOGO + TITRE ET SOUS-TITRE + DATE ===== -->
    <table class="sim-row" width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color: #FFFFFF;" data-id="1">
      <tr>
        <td class="em_side_space">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td>
                <table style="background-color: #FFFFFF;" width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td style="border-top: 3px solid #177fc1;">&nbsp;</td>
                  </tr>
                  <tr>
                    <td>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color: #FFFFFF;">
                        <tr>
                          <td>
                            <table width="150" border="0" cellspacing="0" cellpadding="0" align="left" class="em_wrapper">
                              <tr>
                                <td>
                                  <table width="140" border="0" cellspacing="0" cellpadding="0" align="center">
                                    <tr>
                                      <td align="center">
                                        <img src="http://file.splio3.fr/6f4/T7/2V89/logo_cipres.jpg" width="90" alt="CIPRÉS Assurances" style="border: 0;">
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                            <table width="390" border="0" cellspacing="0" cellpadding="0" align="left" class="em_wrapper" style="background-color: #FFFFFF;">
                              <tr>
                                <td class="em_pad_top">
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                      <td class="em_dark_grey_txt" style="font-family:Arial, sans-serif;font-size:20px;text-align:left;color:#666666;line-height:23px;">
                                        <h1 class="titre_accroche sim-row-edit" data-type="title"  style="color: #177fc1;font-size: 32px; line-height: 50px; font-weight: 700;margin: 0;"><span style="text-transform: uppercase;">CIPR&Eacute;S</span> DE NOUS N°1</h1>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td class="em_grey_txt" style="font-family:Arial, sans-serif;font-size:13px;text-align:left;color:#666666;line-height:22px;">
                                        <h2 class="soustitre_accroche sim-row-edit" data-type="title"  style="color: #666666;font-weight: 500;margin: 0; font-size: 20px;">Notre rendez-vous mensuel d'informations</h2>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td height="12" style="line-height:1px;font-size:1px;">&nbsp;
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                            <table width="150" border="0" cellspacing="0" cellpadding="0" align="right" class="em_wrapper">
                              <tr>
                                <td class="em_pad_top" align="center">
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                      <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td class="em_grey_txt sim-row-edit" data-type="title" align="center" style="font-family:Arial, sans-serif;font-size:20px;color:#666666;line-height:22px; font-weight: 500; text-transform: uppercase;">fÉvrier<br>2016</td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td height="20">&nbsp;
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

    <!-- SPACER -->
    <table class="sim-row" width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse;" data-id="2">
        <tr>
          <td height="20" style="line-height:1px;font-size:1px; background-color: #F5F5F5;">&nbsp;</td>
        </tr>
    </table>

    <!-- ===== BLOCS EDITO + AGENDA ===== -->
    <table class="sim-row" width="100%" border="0" cellspacing="0" cellpadding="0" data-id="3">
      <tr>
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="em_side_space">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td>
                      <table width="448" border="0" cellspacing="0" cellpadding="0" align="left" class="em_wrapper" style="background-color: #FFFFFF;">
                        <tr>
                          <td class="sim-row-edit" data-type="imagetocrop" colspan="2"><img src="http://file.splio3.fr/6f4/T7/2V89/visuel_edito.jpg" alt="" /></td>
                        </tr>
                        <tr>
                          <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                          <td class="em_dark_grey_txt" style="font-family:Arial, sans-serif;font-size:20px;text-align:left;color:#177fc1;line-height:23px; padding-left: 20px; font-weight: bold;">
                            <table>
                              <tr>
                                <td class="sim-row-edit" data-type="icon"><img src="http://file.splio3.fr/6f4/T7/2V89/picto_edito.png" alt="" /></td>
                                <td>&nbsp;</td>
                                <td class="em_grey_txt sim-row-edit" data-type="title" style="font-family:Arial, sans-serif;font-size:20px;color:#177fc1;line-height:22px; font-weight: 600; text-transform: uppercase;">Édito</td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td colspan="2" class="em_dark_grey_txt sim-row-edit" data-type="text" style="font-family:Arial, sans-serif;font-size:13px;text-align:left;color:#666666; padding: 20px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. <br>
                            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                          </td>
                        </tr>
                        <tr>
                          <td colspan="2" style="padding: 0 20px 20px 0;">
                          <table align="right" width="150" height="30" class="btn" style="border-spacing: 0;font-family: sans-serif;color: #7c7c7c;font-size: 13px;background-color: #FFFFFF;">
                              <tr>
                                  <td align="center" width="150" height="30" style="text-align: center;padding: 0; border-bottom: 2px solid #EEEEEE; background-color: #177fc1;">
                                      <a class="sim-row-edit" data-type="btn" href="http://www.cipres.fr/Cipres-News/Newsletter-012816?content=actu2&utm_source=splio&utm_medium=email&utm_campaign=newsletter_012816" style="color: #FFFFFF;text-decoration: none;width: 150px;font-family: Arial,Helvetica,sans-serif;display: block;background-color: #177fc1;text-align: center;font-size: 14px;text-transform: uppercase;font-weight: 700;line-height: 30px;" target="_blank">Lire la suite</a>
                                  </td>
                              </tr>
                          </table>
                          </td>
                        </tr>
                      </table>

                      <table width="20" height="330" align="left" style="background-color: #F5F5F5;">
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                      </table>

                      <!-- BLOC AGENDA -->
                      <table style="background-color: #FFFFFF;" width="225" height="330" border="0" cellspacing="0" cellpadding="0" align="right" class="em_wrapper">
                        <tr>
                          <td width="100%" height="320" class="em_pad_top" style="padding: 0 10px 10px 15px;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="30" style="padding-top: 12px;" class="sim-row-edit" data-type="image"><img src="http://file.splio3.fr/6f4/T7/2V89/picto_calendar.png" alt="" /></td>
                                <td class="em_dark_grey_txt" style="font-family:Arial, sans-serif;font-size:20px;text-align:left;color:#666666;line-height:23px; padding-top: 15px;">
                                  <h3 class="titre_article sim-row-edit" data-type="title" style="color: #ED9E0B;font-size: 18px;text-transform: uppercase;font-weight: 700;margin: 0;">NE PAS MANQUER</h3>
                                </td>
                              </tr>
                              <tr>
                                <td height="25" colspan="2">&nbsp;</td>
                              </tr>
                              <tr>
                                <td colspan="2">
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse; border-left: 2px solid #ED9E0B; font-family:Arial, sans-serif;font-size:16px; color: #666666;">
                                    <tr>
                                      <td class="sim-row-edit" data-type="text" style="padding-left: 10px;"><strong>18 février</strong></td>
                                    </tr>
                                    <tr>
                                      <td class="sim-row-edit" data-type="text" style="padding-left: 10px;">Matinale d'info</td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td height="40" colspan="2">&nbsp;</td>
                              </tr>
                              <tr>
                                <td colspan="2">
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse; border-left: 2px solid #ED9E0B; font-family:Arial, sans-serif;font-size:16px; color: #666666;">
                                    <tr>
                                      <td class="sim-row-edit" data-type="text" style="padding-left: 10px;"><strong>22 février</strong></td>
                                    </tr>
                                    <tr>
                                      <td class="sim-row-edit" data-type="text" style="padding-left: 10px;">Prochaine DUP</td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td height="40" colspan="2">&nbsp;</td>
                              </tr>
                              <tr>
                                <td colspan="2">
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse; border-left: 2px solid #ED9E0B; font-family:Arial, sans-serif;font-size:16px; color: #666666;">
                                    <tr>
                                      <td class="sim-row-edit" data-type="text" style="padding-left: 10px;"><strong>19 mars</strong></td>
                                    </tr>
                                    <tr>
                                      <td class="sim-row-edit" data-type="text" style="padding-left: 10px;">Buffet DSC mobilisation</td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td height="40" colspan="2">&nbsp;</td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

    <!-- ===== BLOCS CHIFFRES (X4) ===== -->
    <table class="sim-row" width="100%" border="0" cellspacing="0" cellpadding="0" data-id="4">
      <tr>
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="em_side_space">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="100%" class="em_dark_grey_txt" style="font-family:Arial, sans-serif;font-size:20px;text-align:left;color:#177fc1;line-height:23px; font-weight: bold;">
                      <table bgcolor="F5F5F5">
                        <tr>
                          <td class="sim-row-edit" data-type="icon"><img src="http://file.splio3.fr/6f4/T7/2V89/picto_chiffres_cles.png" alt="" /></td>
                          <td>&nbsp;</td>
                          <td class="em_grey_txt sim-row-edit" data-type="title" style="font-family:Arial, sans-serif;font-size:20px;color:#177fc1;line-height:22px; font-weight: 600; text-transform: uppercase;">Nos chiffres clés</td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td width="100%" height="20" style="line-height:1px;font-size:1px; background-color: #F5F5F5;">&nbsp;</td>
                  </tr>
                  <tr>
                    <td width="100%">
                      <!-- 1er BLOC CHIFFRES -->
                      <table class="bloc_chiffres_rouge sim-row-bg-edit" data-type="bg_bloc_chiffre" style="background-color: #ff3c1f; color: #FFFFFF; font-family: Arial, sans-serif; text-align: center; font-size:14px; border-collapse: collapse;" width="155" align="left" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td class="sim-row-edit" data-type="text" style="font-size: 42px; padding-top: 10px;">144</td>
                        </tr>
                        <tr>
                          <td style="font-size: 16px; font-weight: bold; text-transform: uppercase;" class="sim-row-edit" data-type="text">millions €<br>de CA</td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td class="sim-row-edit" data-type="pictos_chiffres"><img src="http://file.splio3.fr/6f4/T7/2V89/picto_ca.png" alt="" /></td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                      </table>
                      <table align="left" width="22">
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                      </table>

                      <!-- 2eme BLOC CHIFFRES -->
                      <table class="bloc_chiffres_rouge sim-row-bg-edit" data-type="bg_bloc_chiffre" style="background-color: #177fc1; color: #FFFFFF; font-family: Arial, sans-serif; text-align: center; font-size:14px; border-collapse: collapse;" width="155" align="left" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td class="sim-row-edit" data-type="text" style="font-size: 42px; padding-top: 10px;">16 700</td>
                        </tr>
                        <tr>
                          <td class="sim-row-edit" data-type="text" style="font-size: 16px; font-weight: bold; text-transform: uppercase;">Heures de <br>formation</td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td class="sim-row-edit" data-type="pictos_chiffres"><img src="http://file.splio3.fr/6f4/T7/2V89/picto_formation.png" alt="" /></td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                      </table>
                      <table align="left" width="22">
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                      </table>

                      <!-- 3eme BLOC CHIFFRES -->
                      <table class="bloc_chiffres_rouge sim-row-bg-edit" data-type="bg_bloc_chiffre" style="background-color: #23cacc; color: #FFFFFF; font-family: Arial, sans-serif; text-align: center; font-size:14px; border-collapse: collapse;" width="155" align="left" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td class="sim-row-edit" data-type="text" style="font-size: 42px; padding-top: 10px;">840</td>
                        </tr>
                        <tr>
                          <td class="sim-row-edit" data-type="text" style="font-size: 16px; font-weight: bold; text-transform: uppercase;">Partenaires<br>en France</td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td class="sim-row-edit" data-type="pictos_chiffres"><img src="http://file.splio3.fr/6f4/T7/2V89/picto_collaborateurs.png" alt="" /></td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                      </table>
                      <table align="left" width="22">
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                      </table>

                      <!-- 4eme BLOC CHIFFRES -->
                      <table class="bloc_chiffres_rouge sim-row-bg-edit" data-type="bg_bloc_chiffre" style="background-color: #f8d90c; color: #FFFFFF; font-family: Arial, sans-serif; text-align: center; font-size:14px; border-collapse: collapse;" width="155" align="left" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td class="sim-row-edit" data-type="text" style="font-size: 42px; padding-top: 10px;">200</td>
                        </tr>
                        <tr>
                          <td class="sim-row-edit" data-type="text" style="font-size: 16px; font-weight: bold; text-transform: uppercase;">Nouveaux<br>logements</td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td class="sim-row-edit" data-type="pictos_chiffres"><img src="http://file.splio3.fr/6f4/T7/2V89/picto_logements.png" alt="" /></td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

    <!-- ===== BLOCS CHIFFRES (X2) ===== -->
    <table class="sim-row" width="50%" border="0" cellspacing="0" cellpadding="0" data-id="4.2">
      <tr>
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="em_side_space">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="100%" class="em_dark_grey_txt" style="font-family:Arial, sans-serif;font-size:20px;text-align:left;color:#177fc1;line-height:23px; font-weight: bold;">
                      <table bgcolor="F5F5F5">
                        <tr>
                          <td class="sim-row-edit" data-type="icon"><img src="http://file.splio3.fr/6f4/T7/2V89/picto_chiffres_cles.png" alt="" /></td>
                          <td>&nbsp;</td>
                          <td class="em_grey_txt sim-row-edit" data-type="title" style="font-family:Arial, sans-serif;font-size:20px;color:#177fc1;line-height:22px; font-weight: 600; text-transform: uppercase;">Nos chiffres clés</td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td width="100%" height="20" style="line-height:1px;font-size:1px; background-color: #F5F5F5;">&nbsp;</td>
                  </tr>
                  <tr>
                    <td width="100%">
                      <!-- 1er BLOC CHIFFRES -->
                      <table class="bloc_chiffres_rouge sim-row-bg-edit" data-type="bg_bloc_chiffre" style="background-color: #ff3c1f; color: #FFFFFF; font-family: Arial, sans-serif; text-align: center; font-size:14px; border-collapse: collapse;" width="155" align="left" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td class="sim-row-edit" data-type="text" style="font-size: 42px; padding-top: 10px;">144</td>
                        </tr>
                        <tr>
                          <td style="font-size: 16px; font-weight: bold; text-transform: uppercase;" class="sim-row-edit" data-type="text">millions €<br>de CA</td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td class="sim-row-edit" data-type="pictos_chiffres"><img src="http://file.splio3.fr/6f4/T7/2V89/picto_ca.png" alt="" /></td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                      </table>
                      <table align="left" width="22">
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                      </table>

                      <!-- 2eme BLOC CHIFFRES -->
                      <table class="bloc_chiffres_rouge sim-row-bg-edit" data-type="bg_bloc_chiffre" style="background-color: #177fc1; color: #FFFFFF; font-family: Arial, sans-serif; text-align: center; font-size:14px; border-collapse: collapse;" width="155" align="left" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td class="sim-row-edit" data-type="text" style="font-size: 42px; padding-top: 10px;">16 700</td>
                        </tr>
                        <tr>
                          <td class="sim-row-edit" data-type="text" style="font-size: 16px; font-weight: bold; text-transform: uppercase;">Heures de <br>formation</td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td class="sim-row-edit" data-type="pictos_chiffres"><img src="http://file.splio3.fr/6f4/T7/2V89/picto_formation.png" alt="" /></td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                      </table>
                      <table align="left" width="22">
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

    <!-- ===== BLOC ARTICLE 100% ===== -->
    <table class="sim-row" width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color: #FFFFFF;" data-id="5">
      <tr>
        <td class="sim-row-edit" data-type="imagetocrop" width="270"><img src="http://file.splio3.fr/6f4/T7/2V89/visuel_expert.jpg" alt="" /></td>
        <td valign="top">
          <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial, sans-serif;">
            <tr>
              <td class="em_dark_grey_txt" style="font-family:Arial, sans-serif;font-size:20px;text-align:left; line-height:23px; font-weight: bold; padding-top: 10px;">
                <table>
                  <tr>
                    <td class="sim-row-edit" data-type="icon"><img src="http://file.splio3.fr/6f4/T7/2V89/picto_speak.png" alt="" /></td>
                    <td>&nbsp;</td>
                    <td class="em_grey_txt sim-row-edit" data-type="title" style="font-family:Arial, sans-serif;font-size:20px;color:#1C476E;line-height:22px; font-weight: 600; text-transform: uppercase;">Parole d'experts</td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td valign="top" class="em_dark_grey_txt sim-row-edit" data-type="title" style="font-family:Arial, sans-serif;font-size:18px;text-align:left;color:#666666;line-height:23px; padding-right: 15px; padding-top:10px;">Le point ce mois-ci sur Lorem Ipsum</td>
            </tr>
            <tr>
              <td height="85" valign="top" class="em_dark_grey_txt sim-row-edit" data-type="text" style="font-family:Arial, sans-serif;font-size:13px;text-align:left;color:#666666;line-height:23px; padding-right: 15px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut</td>
            </tr>
            <tr>
                <td colspan="2" style="padding: 0 20px 0 0;">
                <table align="right" width="150" height="30" class="btn" style="border-spacing: 0;font-family: sans-serif;color: #7c7c7c;font-size: 13px;background-color: #FFFFFF;">
                    <tr>
                        <td align="center" width="150" height="30" style="text-align: center;padding: 0; border-bottom: 2px solid #EEEEEE; background-color: #1c476e;">
                            <a class="sim-row-edit" data-type="btn" href="" style="color: #FFFFFF;text-decoration: none;width: 150px;font-family: Arial,Helvetica,sans-serif;display: block;background-color: #1c476e;text-align: center;font-size: 14px;text-transform: uppercase;font-weight: 700;line-height: 30px;" target="_blank">Lire la suite</a>
                        </td>
                    </tr>
                </table>
                </td>
              </tr>
          </table>
        </td>
      </tr>
    </table>

    <!-- ===== BLOC ARTICLE 100% SANS BOUTON ===== -->
    <table class="sim-row" width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color: #FFFFFF;" data-id="6">
      <tr>
        <td class="sim-row-edit" data-type="imagetocrop" width="270"><img src="http://file.splio3.fr/6f4/T7/2V89/visuel_rdv.jpg" alt="" /></td>
        <td valign="top">
          <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial, sans-serif;">
            <tr>
              <td class="em_dark_grey_txt" style="font-family:Arial, sans-serif;font-size:20px;text-align:left; line-height:23px; font-weight: bold; padding-top: 10px;">
                <table>
                  <tr>
                    <td class="sim-row-edit" data-type="picto"><img src="http://file.splio3.fr/6f4/T7/2V89/picto_rdv.png" alt="" /></td>
                    <td>&nbsp;</td>
                    <td class="em_grey_txt sim-row-edit" data-type="title" style="font-family:Arial, sans-serif;font-size:20px;color:#FF29FF;line-height:22px; font-weight: 600; text-transform: uppercase;">RDV au Happy Place</td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td valign="top" class="em_dark_grey_txt sim-row-edit" data-type="title" style="font-family:Arial, sans-serif;font-size:18px;text-align:left;color:#666666;line-height:23px; padding-right: 15px; padding-top:10px;">Le Happy Place met à l'honneur Arnaud Pavic</td>
            </tr>
            <tr>
              <td height="85" valign="top" class="em_dark_grey_txt sim-row-edit" data-type="text" style="font-family:Arial, sans-serif;font-size:13px;text-align:left;color:#666666;line-height:23px; padding-right: 15px;">Vous êtes impliqués personnellement dans un domaine culturel, humanitaire, social, sportif, artistique ? <br>
              Vous souhaitez le partager avec vos collègues de l'entreprise ? <br><br>

              Faîtes-le nous savoir en écrivant à : drh@ciprés.fr</td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

    <!-- ===== BLOC 2 ARTICLES ===== -->
    <table class="sim-row" width="100%" border="0" cellspacing="0" cellpadding="0" data-id="7">
      <tr>
          <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td class="em_side_space">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <table width="340" height="330" border="0" cellspacing="0" cellpadding="0" align="left" class="em_wrapper" style="background-color: #FFFFFF;">
                            <tr>
                              <td class="em_dark_grey_txt" style="font-family:Arial, sans-serif;font-size:20px;text-align:left;color:#6944e2;line-height:23px; font-weight: bold; padding-top: 10px;">
                                <table width="100%" style="border-collapse: collapse;">
                                  <tr>
                                    <td class="em_dark_grey_txt" style="font-family:Arial, sans-serif; text-align:left; line-height:23px; font-weight: bold; padding-top: 10px; padding-left: 15px;">
                                      <table>
                                        <tr>
                                          <td class="sim-row-edit" data-type="icon"><img src="http://file.splio3.fr/6f4/T7/2V89/picto_people.png" alt="" /></td>
                                          <td>&nbsp;</td>
                                          <td class="em_grey_txt sim-row-edit" data-type="title" style="font-family:Arial, sans-serif;font-size:20px;color:#6944e2;line-height:22px; font-weight: 600; text-transform: uppercase;">Carnets people</td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td height="40" valign="top" class="em_dark_grey_txt sim-row-edit" data-type="title" style="font-family:Arial, sans-serif;font-size:16px;text-align:left;color:#666666; padding-left: 15px;">Des bébés</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <table width="49%" align="left" style="font-family:Arial, sans-serif;font-size:13px; color: #666666; text-align:center;">
                                        <tr>
                                          <td><img src="http://file.splio3.fr/6f4/T7/2V89/people1.jpg" alt="" /></td>
                                        </tr>
                                        <tr>
                                          <td class="sim-row-edit" data-type="text">Xavier Le Cousse pour <br>XX le X janvier 2016 </td>
                                        </tr>
                                      </table>

                                      <table width="49%" align="right" style="font-family:Arial, sans-serif;font-size:13px; color: #666666; text-align:center;">
                                        <tr>
                                          <td><img src="http://file.splio3.fr/6f4/T7/2V89/people2.jpg" alt="" /></td>
                                        </tr>
                                        <tr>
                                          <td class="sim-row-edit" data-type="text">Karine Pelherbe pour <br>XX le X janvier 2016 </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>&nbsp;</td>
                                  </tr>
                                  <tr>
                                      <td colspan="2" style="padding: 0 20px 20px 0;">
                                      <table align="right" width="150" height="30" class="btn" style="border-spacing: 0;font-family: sans-serif;color: #7c7c7c;font-size: 13px;background-color: #FFFFFF;">
                                          <tr>
                                              <td align="center" width="150" height="30" style="text-align: center;padding: 0; border-bottom: 2px solid #EEEEEE; background-color: #6944e2;">
                                                  <a class="sim-row-edit" data-type="btn" href="http://www.cipres.fr/Cipres-News/Newsletter-012816?content=actu2&utm_source=splio&utm_medium=email&utm_campaign=newsletter_012816" style="color: #FFFFFF;text-decoration: none;width: 150px;font-family: Arial,Helvetica,sans-serif;display: block;background-color: #6944e2;text-align: center;font-size: 14px;text-transform: uppercase;font-weight: 700;line-height: 30px;" target="_blank">Lire la suite</a>
                                              </td>
                                          </tr>
                                      </table>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                            <table width="340" height="330" border="0" cellspacing="0" cellpadding="0" align="right" class="em_wrapper" style="background-color: #FFFFFF;">
                            <tr>
                              <td class="em_dark_grey_txt" style="font-family:Arial, sans-serif;font-size:20px;text-align:left;color:#23cacc;line-height:23px; font-weight: bold; padding-top: 10px;" valign="top">
                                <table width="100%" style="border-collapse: collapse;">
                                  <tr>
                                    <td class="em_dark_grey_txt" style="font-family:Arial, sans-serif; text-align:left; line-height:23px; font-weight: bold; padding-top: 10px; padding-left: 15px;">
                                      <table>
                                        <tr>
                                          <td class="sim-row-edit" data-type="icon"><img src="http://file.splio3.fr/6f4/T7/2V89/picto_programme.png" alt="" /></td>
                                          <td>&nbsp;</td>
                                          <td class="em_grey_txt sim-row-edit" data-type="title" style="font-family:Arial, sans-serif;font-size:20px;color:#23cacc;line-height:22px; font-weight: 600; text-transform: uppercase;">Programme</td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td align="right" style="padding-right: 15px;"><img src="http://file.splio3.fr/6f4/T7/2V89/image_prog_sante.jpg" alt="" /></td>
                                  </tr>
                                  <tr>
                                    <td>&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td class="sim-row-edit" data-type="text" style="color: #666666; font-size: 13px; font-weight: normal; padding: 0 15px; font-family: Arial, sans-serif;">Consultez votre nouveau site de gestion mutuelle <br><br>

                                    Les inspecteurs sensibilisés aux risques routiers&nbsp;:<br> 
                                    le 24 toute une équipe du centre de médecine sont venus les sensibiliser<br><br></td>
                                  </tr>
                                  <tr>
                                    <td>&nbsp;</td>
                                  </tr>
                                  <tr>
                                      <td colspan="2" style="padding: 0 20px 20px 0;">
                                      <table align="right" width="150" height="30" class="btn" style="border-spacing: 0;font-family: sans-serif;color: #7c7c7c;font-size: 13px;background-color: #FFFFFF;">
                                          <tr>
                                              <td align="center" width="150" height="30" style="text-align: center;padding: 0; border-bottom: 2px solid #EEEEEE; background-color: #23cacc;">
                                                  <a class="sim-row-edit" data-type="btn" href="http://www.cipres.fr/Cipres-News/Newsletter-012816?content=actu2&utm_source=splio&utm_medium=email&utm_campaign=newsletter_012816" style="color: #FFFFFF;text-decoration: none;width: 150px;font-family: Arial,Helvetica,sans-serif;display: block;background-color: #23cacc;text-align: center;font-size: 14px;text-transform: uppercase;font-weight: 700;line-height: 30px;" target="_blank">Lire la suite</a>
                                              </td>
                                          </tr>
                                      </table>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="20">
                        </td>
                    </tr>
                    </table>
                </td>
              </tr>
              </table>
          </td>
      </tr>
    </table>
    
    <!-- ===== FOOTER ===== -->
    <table class="sim-row" bgcolor="FFFFFF" width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color: #FFFFFF;" data-id="8">
                  <tr>
                    <td class="em_side_space">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td>&nbsp;</td>
                              </tr>
                              <tr>
                                <td>
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                      <td>
                                        <table width="150" border="0" cellspacing="0" cellpadding="0" align="left" class="em_wrapper">
                                          <tr>
                                            <td>
                                              <table width="140" border="0" cellspacing="0" cellpadding="0" align="center">
                                                <tr>
                                                  <td align="center">
                                                    <img src="http://file.splio3.fr/6f4/T7/2V89/logo_cipres.jpg" width="65" alt="CIPRÉS Assurances" style="border: 0;">
                                                  </td>
                                                </tr>
                                              </table>
                                            </td>
                                          </tr>
                                        </table>
                                        <table width="540" border="0" cellspacing="0" cellpadding="0" align="left" class="em_wrapper">
                                          <tr>
                                            <td class="em_pad_top">
                                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                  <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                  <td class="em_grey_txt" style="font-family:Arial, sans-serif;font-size:13px;text-align:left;color:#666666;">
                                                    Une suggestion, un commentaire ? Envoyez nous un mail à  cette adresse : <strong>drh@cipres.fr</strong>
                                                  </td>
                                                </tr>
                                              </table>
                                            </td>
                                          </tr>
                                        </table>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td style="border-bottom: 3px solid #177fc1;">&nbsp;</td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
    </table>





</div>