<?php
	header('Content-Type: text/html; charset=UTF-8');

	// Sauvegarde de la session
	if (isset($_POST['contenu_session'])) {
		$contenu_session = $_POST['contenu_session'];
		$nom_session = $_POST['nom_session'];
		$date_session = date('Y-m-d H:i:s');
		echo $contenu_session;

		require_once 'includes/DB_connect.php';

		$req = $bdd->prepare('INSERT INTO saved_sessions (contenu_session, nom_session, date_session) VALUES(:contenu_session, :nom_session, :date_session)');
	    $req->execute(array(
	      'contenu_session'   => $contenu_session,
	      'nom_session'   => $nom_session, 
	      'date_session'  => $date_session
	    ));
	}

	// Génération du select pour charger une session enregistrée
	$ops='';
	$pdo = new PDO('mysql:host=localhost;dbname=newsletter_interne', 'root', '');
	$stmt = $pdo->query("SELECT nom_session FROM saved_sessions");

	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	    $ops.= "<option value='" . $row['nom_session'] . "'>" . $row['nom_session'] . "</option>";
	}

	// Chargement du modèle de mail selon la session chargée
	if (isset($_POST['sessionToLoad'])) {
		$contenu_session_chargee = '';
		$pdo = new PDO('mysql:host=localhost;dbname=newsletter_interne', 'root', '');

		$choix_session = $_POST['sessionToLoad'];

		$bdd = $pdo->query("SELECT nom_session, contenu_session FROM saved_sessions WHERE nom_session = '$choix_session' ");

		while ($row = $bdd->fetch(PDO::FETCH_ASSOC)) {
		    $contenu_session_chargee.= $row['contenu_session'];
		    echo $contenu_session_chargee;
		}
	}
	

?>