$(function () {

  'use strict';

  var console = window.console || { log: function () {} };
  var $image = $('#image');
  var $download = $('#download');
  var $dataX = $('#dataX');
  var $dataY = $('#dataY');
  var $dataHeight = $('#dataHeight');
  var $dataWidth = $('#dataWidth');
  var $dataRotate = $('#dataRotate');
  var $dataScaleX = $('#dataScaleX');
  var $dataScaleY = $('#dataScaleY');
  var options = {
        preview: '.img-preview',
        aspectRatio: NaN,
        crop: function (e) {
          $dataX.val(Math.round(e.x));
          $dataY.val(Math.round(e.y));
          $dataHeight.val(Math.round(e.height));
          $dataWidth.val(Math.round(e.width));
          $dataRotate.val(e.rotate);
          $dataScaleX.val(e.scaleX);
          $dataScaleY.val(e.scaleY);
        }
      };


  // Tooltip
  $('[data-toggle="tooltip"]').tooltip();


  // Cropper
  $image.on({
    'build.cropper': function (e) {
      console.log(e.type);
    },
    'built.cropper': function (e) {
      console.log(e.type);
    },
    'cropstart.cropper': function (e) {
      console.log(e.type, e.action);
    },
    'cropmove.cropper': function (e) {
      console.log(e.type, e.action);
    },
    'cropend.cropper': function (e) {
      console.log(e.type, e.action);
    },
    'crop.cropper': function (e) {
      console.log(e.type, e.x, e.y, e.width, e.height, e.rotate, e.scaleX, e.scaleY);
    },
    'zoom.cropper': function (e) {
      console.log(e.type, e.ratio);
    }
  }).cropper(options);


  // Buttons
  if (!$.isFunction(document.createElement('canvas').getContext)) {
    $('button[data-method="getCroppedCanvas"]').prop('disabled', true);
  }

  if (typeof document.createElement('cropper').style.transition === 'undefined') {
    $('button[data-method="rotate"]').prop('disabled', true);
    $('button[data-method="scale"]').prop('disabled', true);
  }


  // Download
  if (typeof $download[0].download === 'undefined') {
    $download.addClass('disabled');
  }


  // Options
  $('.docs-toggles').on('change', 'input', function () {
    var $this = $(this);
    var name = $this.attr('name');
    var type = $this.prop('type');
    var cropBoxData;
    var canvasData;

    if (!$image.data('cropper')) {
      return;
    }

    if (type === 'checkbox') {
      options[name] = $this.prop('checked');
      cropBoxData = $image.cropper('getCropBoxData');
      canvasData = $image.cropper('getCanvasData');

      options.built = function () {
        $image.cropper('setCropBoxData', cropBoxData);
        $image.cropper('setCanvasData', canvasData);
      };
    } else if (type === 'radio') {
      options[name] = $this.val();
    }

    $image.cropper('destroy').cropper(options);
  });


  // Methods
  $('.docs-buttons').on('click', '[data-method]', function () {
    var $this = $(this);
    var data = $this.data();
    var $target;
    var result;

    if ($this.prop('disabled') || $this.hasClass('disabled')) {
      return;
    }

    if ($image.data('cropper') && data.method) {
      data = $.extend({}, data); // Clone a new one

      if (typeof data.target !== 'undefined') {
        $target = $(data.target);

        if (typeof data.option === 'undefined') {
          try {
            data.option = JSON.parse($target.val());
          } catch (e) {
            console.log(e.message);
          }
        }
      }

      result = $image.cropper(data.method, data.option, data.secondOption);

      switch (data.method) {
        case 'scaleX':
        case 'scaleY':
          $(this).data('option', -data.option);
          break;

        case 'getCroppedCanvas':
          if (result) {

            // Bootstrap's Modal
            $('#getCroppedCanvasModal').modal().find('.modal-body').html(result);

            if (!$download.hasClass('disabled')) {
              // $download.attr('href', result.toDataURL());

              /*=====================================================================
              =            ENVOI DE L'IMAGE SUR LE SERVEUR APRES LE CROP            =
              =====================================================================*/
              $download.click(function(){
                $.ajax({
                    type: "POST",
                    url: "/generateur_emails_interne/php/save.php",
                    data: { image_to_save: result.toDataURL({format: 'png', quality: 10}) },
                    success: function(url_def){
                        // Fermeture de la modal box
                        // $('.modal-dialog, .modal-backdrop, #sim-edit-crop-image').fadeOut(500);

                        // Récupère l'url de l'image sur le serveur
                        var url_img_def = url_def.slice(3);
                        big_parent.children('img').attr("src","http://127.0.0.1/generateur_emails_interne/" + url_img_def);
                    }
                });  
              });
            }
          }

          break;
      }

      if ($.isPlainObject(result) && $target) {
        try {
          $target.val(JSON.stringify(result));
        } catch (e) {
          console.log(e.message);
        }
      }

    }
  });


  // Keyboard
  $(document.body).on('keydown', function (e) {

    if (!$image.data('cropper') || this.scrollTop > 300) {
      return;
    }

    switch (e.which) {
      case 37:
        e.preventDefault();
        $image.cropper('move', -1, 0);
        break;

      case 38:
        e.preventDefault();
        $image.cropper('move', 0, -1);
        break;

      case 39:
        e.preventDefault();
        $image.cropper('move', 1, 0);
        break;

      case 40:
        e.preventDefault();
        $image.cropper('move', 0, 1);
        break;
    }

  });


  // Import image
  var $inputImage = $('#inputImage');
  var URL = window.URL || window.webkitURL;
  var blobURL;

  if (URL) {
    $inputImage.change(function () {
      var files = this.files;
      var file;

      if (!$image.data('cropper')) {
        return;
      }

      if (files && files.length) {
        file = files[0];

        if (/^image\/\w+$/.test(file.type)) {
          blobURL = URL.createObjectURL(file);
          $image.one('built.cropper', function () {

            // Revoke when load complete
            URL.revokeObjectURL(blobURL);
          }).cropper('reset').cropper('replace', blobURL);
          $inputImage.val('');
        } else {
          window.alert('Please choose an image file.');
        }
      }
    });
  } else {
    $inputImage.prop('disabled', true).parent().addClass('disabled');
  }


  /*==============================================
  =            AJOUTS FONCTIONNALITES            =
  ==============================================*/

  // Enregistrement des sessions
  $('#newsletter-builder-save-session').click(function(){
    // Pronpt pour nommer la session
    var nom_session = prompt("Veuillez donner un nom à votre projet", "");

    if (nom_session != null) {

      // ENVOI DES DONNEES EN AJAX VERS LA BASE
      var contenu_recupere = $('#newsletter-builder-area-center-frame-content').html();
      $.ajax({
        type: "POST",
        contentType: "application/x-www-form-urlencoded;charset=UTF-8",
        url: "/generateur_emails_interne/php/scripts.php",
        data: { contenu_session: contenu_recupere, nom_session : nom_session},

        // success: function(url_def){
        //     // Fermeture de la modal box
        //     // $('.modal-dialog, .modal-backdrop, #sim-edit-crop-image').fadeOut(500);

        //     // Récupère l'url de l'image sur le serveur
        //     var url_img_def = url_def.slice(3);
        //     big_parent.children('img').attr("src","http://127.0.0.1/generateur_emails_interne/" + url_img_def);
        // }
      });
    };
  });

  // Chargement des sessions
  $('#get_session').on('change', function(){
    var selected_session = $('#get_session').val();

    $.ajax({
      type: "POST",
      contentType: "application/x-www-form-urlencoded;charset=UTF-8",
      url: "/generateur_emails_interne/php/scripts.php",
      data: { sessionToLoad: selected_session},

      success: function(contenu_session){
        var contenu_session_chargee = contenu_session;
        $('#newsletter-builder-area-center-frame-content').html(contenu_session_chargee);
        $.getScript('/generateur_emails_interne/_scripts/newsletter-builder.js');
      }
    });
  });

});
